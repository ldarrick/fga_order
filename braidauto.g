# Filename: braidauto.g
#
# Description: This file contains the function that takes in an integer (which braid group) and a braid and outputs the automorphism that corresponds to it.


# Function: GeneratorBraidAutomorphism
# Description: This function takes in a free group and an integer representing a braid group
# generator. A negative number represents the inverse generator. This function then returns
# the automorphism corresponding to that braid group generator.

GeneratorBraidAutomorphism:= function(FG, b)
	local hom, i, n, autImg, inv;

	n:= RankOfFreeGroup(FG);
	autImg:= [];

	# Send error if braid generator larger than rank
	if(b > n-1) or (b < (-(n-1))) then
		Error("Not a valid braid group generator.");
	fi;

	# Return identity if b=0
	if(b = 0) then
		hom := IdentityMapping(FG);
		return hom;
	fi;

	# Set inverse if b < 0
	if(b < 0) then
		inv := 1;
		b := -b;
	else
		inv := 0;
	fi;

	# Construct the automorphism
	for i in [1..n] do
		if i = b then
			Add(autImg,FG.(i)*FG.(i+1)*FG.(i)^(-1));
		elif i = b+1 then
			Add(autImg,FG.(b));
		else
			Add(autImg,FG.(i));
		fi;
	od;

	hom:= FreeGroupEndomorphismByImages(FG,autImg);

	if( inv = 1 ) then
		hom := Inverse(hom);
	fi;

	return hom;
end;

# Function: BraidAutomorphism
# Description: This function takes in a free group and a list of integers representing an element
# of the braid group. It then returns the automorphism corresponding to this braid.

BraidAutomorphism := function(FG, B)
	local i, hom, homg, n, bl;

	n := RankOfFreeGroup(FG);
	bl := Length(B);

	hom := GeneratorBraidAutomorphism(FG,B[1]);

	for i in [2..bl] do
		homg := GeneratorBraidAutomorphism(FG,B[i]);
		hom := hom*homg;
	od;

	return hom;
end;
		