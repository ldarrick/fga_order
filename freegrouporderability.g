
####################################################################################

# Function: BoundListProduct
#
# Description: This function returns a new list that contains both P1 and P2, and also all
# element-wise products of P1*P2. It ensures that the length of each word is less than r.

BoundListProduct := function(P1, P2, r)
	local P, n1, n2, i, j, w;

	# Find the size of each set
	n1 := Size(P1);
	n2 := Size(P2);

	P := [];

	# Add in original elements in P1 and P2 that are in the r-ball
	for i in [1..n1] do
		if(Length(P1[i]) <= r) then
			Add(P, P1[i]);
		fi;
	od;

	for i in [1..n2] do
		if(Length(P2[i]) <= r) then
			Add(P, P2[i]);
		fi;
	od;

	# Take the product of the two elements in P1 and P2
	for i in [1..n1] do
		for j in [1..n2] do
			w := P1[i]*P2[j];

			if(Length(w) <= r) then
				Add(P, w);
			fi;
		od;
	od;

#	Print("Before: ", Length(P), "\n");
#	Print("After: ", Length(DuplicateFreeList(P)), "\n\n");

	return DuplicateFreeList(P);
end;

####################################################################################

# Function: BoundListConjugation
#
# Description: This function returns a new list that contains P, as well as all conjugates
# of D with the free group FG. It ensures that the length of each word is less than r.
# Thus, we need only consider the r-ball, B, of the free group.

BoundListConjugation := function(B, P, D, r)

	local i, j, n1, n2, w;

	# Find the size of each set
	n1 := Size(B);
	n2 := Size(D);

	for i in [1..n1] do
		for j in [1..n2] do

			w := B[i]*D[j]*B[i]^(-1);

			if(Length(w) <= r) then
				Add(P, w);
			fi;
		od;
	od;

#	Print("Before: ", Length(P), "\n");
#	Print("After: ", Length(DuplicateFreeList(P)), "\n\n");

	return DuplicateFreeList(P);
end;


####################################################################################

# CURRENTLY UNUSED
# Function: PlAddList
#
# Description: This function creates the list of lists which are to be added when elements
# are added to P. This is to keep track of the index of the last element with a certain length.

PlAddList := function(r)

	local i, j, k, PlAdd, PlAddc;

	PlAdd := [];

	for i in [1..r] do
		PlAddc := [];

		for j in [1..r] do
			if (j>=i) then
				Add(PlAddc, 1);
			else
				Add(PlAddc, 0);
			fi;
		od;

		Add(PlAdd, PlAddc);
	od;

	return PlAdd;
end;

####################################################################################

# CURRENTLY UNUSED
# Function: PlConstruct
#
# Description: This function constructs the length list for the list P. This assumes that the
# list P is sorted by length. The length list is a list that contains the index where the length
# of elements in P increases.

PlConstruct := function(P, Pl, r)

	local i, Plc;

	for i in [1..r-1] do
		Plc := PositionProperty(P, f-> Length(f) = i+1);

		if (IsBool(Plc)) then
			if (i=1) then
				Pl[i] := 1;
			else
				Pl[i] := Pl[i-1];
			fi;
		else
			Pl[i] := Plc - 1;
		fi;

		Pl[r] := Length(P);
	od;

end;


####################################################################################

# Function: ConstructB
#
# Description: This function constructs the r-ball of the given free group FG. This is
# done by explicitly adding in 

ConstructB := function(FG, r)
	local n, i, j, B, id, expList, numExp, posExp, genList;

	# Begin by adding the identity to B
	id := FG.(1)*FG.(1)^(-1);
	B := [id];

	# Find rank of free group
	n := RankOfFreeGroup(FG);

	# Create a list that will represent the generators of the free group
	# Note: A positive number x represents the x^th generator, and a negative number
	# -x will represent the inverse of the x^th generator. 
	posExp := [1..n];
	Append(posExp, -[1..n]);

	for i in [1..r] do

		# Create the list of possible exponent values for words of length i
		expList := Tuples(posExp, i);
		numExp := NrTuples(posExp, i);

		for j in [1..numExp] do
			# Create the list of generators given the exponent values
			genList := List(expList[j], f-> FG.(AbsInt(f))^(SignInt(f)));

			# Add the product of these generators to B
			Add(B, Product(genList));
		od;
	od;

	return DuplicateFreeList(B);

end;



			
####################################################################################

# Function: ShortestWord
#
# Description: Given two subsets of a free group (B and P), this function returns the shortest
# nonidentity word in B that is not in P or Pinv.

ShortestWord := function(FG, B, P)
	local id, D, Pinv;

	# Initialize identity
	id := FG.(1)*FG.(1)^(-1);

	# Calculate the inverse elements of P
	Pinv := List(P, f -> Inverse(f));

	# Compute all nonidentity elements in D but not P or Pinv
	D := Difference(B, P);
	D := Difference(D, Pinv);
	D := Difference(D, [id]);

	# Sort D by word length
	Sort(D);

	# Return the first element, which is the one with shortest length
	return D[1];
end;

####################################################################################

# Function: ConstructP
#
# Description: This is a recursive function that determines if there exists a possible positive
# cone that is invariant under the automorphism aut within the r-ball. 
#
# Inputs:
# 	FG  - The free group
# 	B   - The r-ball of the free group
#   P   - The current potential positive cone
#	aut - The automorphism
#	r   - The size of the ball we are considering
#	bi  - Set to 1 if testing for bi-orderability and 0 if testing for left-orderability
#
# Output:
# 	true  - When we get that P indeed satisfies the requirements of a positive cone
#	false - When the identity is found in P, meaning P cannot be a positive cone

ConstructP := function(FG, B, P, aut, r, bi)
	local n, lastP, D, id, Pim, Pinv, sw, res1, res2;

	n := RankOfFreeGroup(FG);
	id := FG.(1)*FG.(1)^(-1);

	# Continuously find the BoundListProduct of the list with itself until it doesn't change
	# anymore
	lastP := ShallowCopy(P);

	Pim := Image(aut, P);
	P := BoundListProduct(P, Pim, r);

	P := BoundListProduct(P, P, r);
 
	while (not IsSubset(lastP, P)) do
		D := Difference(P, lastP);
		lastP := ShallowCopy(P);

		P := BoundListProduct(P, D, r);

		if(id in P) then
			return false;
		fi;

		P := BoundListProduct(D, P, r);

		if(id in P) then
			return false;
		fi;

		Pim := Image(aut, D);
		P := BoundListProduct(P, Pim, r);

		if(id in P) then
			return false;
		fi;

		if(bi = 1) then
			P := BoundListConjugation(B, P, D, r);

			if(id in P) then
				return false;
			fi;

		fi;

		#Print("P Length: ", Length(P), "\n");
		#Print("lastP Length: ", Length(lastP), "\n\n");
	od;

	# Calculate the inverse of P
	Pinv := List(P, f -> Inverse(f));

	if (id in P) then
		return false;
	elif (IsSubset(Union(P, Pinv, [id]), B)) then
		Print("Positive Cone: ", P,"\n");
		return true;
	else
		# Add in the shortest word or its inverse and recurse
		sw := ShortestWord(FG, B, P);

		res1 := ConstructP(FG, B, Union(P, [sw]), aut, r, bi);

		# If a positive cone is constructed, then we are done
		if(res1) then
			return res1;
		fi;

		res2 := ConstructP(FG, B, Union(P, [sw^(-1)]), aut, r, bi);

		# If a positive cone does not exist for both cases, then return the added
		# element that maeks it stop
		if((not res1) and (not res2)) then
			Print(sw,"\n");
		fi;


		return res1 or res2;
	fi;

end;

####################################################################################

# Function: FGAPositiveCone
#
# Description: This function checks whether or not there exists a positive cone within the r-ball such that the given automorphism is order preserving
#
# Inputs:
# 	FG  - The free group
#	aut - The automorphism
#	r   - The size of the ball we are considering
#	bi  - Set to 1 if testing for bi-orderability and 0 if testing for left-orderability
#
# Output:
# 	true  - Such a positive cone within the r-ball exists
#	false - Such a positive cone within the r-ball does not exist (automorphism is NOT order preserving)

FGAPositiveCone := function(FG, aut, r, bi)
	local n, B, Bl, P, PlAdd, i, res;

	n := RankOfFreeGroup(FG);

	# Construct the r-ball of the free group
	B := ConstructB(FG, r);

	# Initialize P to just contain the first generator since wlog, we can assume that the
	# first generator is positive.
	P := [FG.(1)];

	return ConstructP(FG, B, P, aut, r, bi);
end;


