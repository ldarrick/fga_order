# Name: example.g
# Description:This is an example which shows that the automorphism corresponding to the braid sigma_1 * sigma_2^(-1) is non orderable.

# Note: Make sure GAP is run from the directory with all required files:
# 	example.g (this one)
#	braidauto.g
#	freegrouporderability.g


# First, read in the code with the required functions.
Read("braidauto.g");
Read("freegrouporderability.g");

# Define the free group
# The argument represents the rank of the free group
FG := FreeGroup(3);

# Define the automorphism induced by the braid sigma_1 * sigma_2^(-1)
# Argument 1: The free group FG
# Argument 2: A list corresponding to the generators of the braid group, so that the braid sigma_2 * sigma_1^(-1) is represented as [1,-2].
aut := BraidAutomorphism(FG, [1,-2]);

# Call FreeGroupOrderability()
# Argument 1: The free group FG
# Argument 2: The automorphism aut
# Argument 3: The size of the ball to consider
# Argument 4: Whether or not to check bi-orderability or left-orderability
# 		      0: Check left-orderabiility
#			  1: Check bi-orderability

orderPreserving := FGAOrderPreserving(FG, aut, 4, 1);
Print(orderPreserving);